package com.xeiot.open.common;

/**
 * 常量定义
 * Created by wangkun23 on 2018/12/19.
 */
public interface Constants {

    /**
     * "身份信息"参数名称
     */
    public static final String PRINCIPAL_ATTRIBUTE_NAME = "ACCOUNT_SESSION_CONFIG_PRINCIPAL_KEY";

    /**
     * "验证码"参数名称
     */
    public static final String KAPTCHA_SESSION_CONFIG_KEY = "KAPTCHA_SESSION_CONFIG_KEY";

    /**
     * jwt加密KEY
     */
    public static final String JWT_KEY = "secret_key";
}
