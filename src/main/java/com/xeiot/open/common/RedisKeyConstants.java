package com.xeiot.open.common;

/**
 * redis key值定义
 * Created by wangkun23 on 2018/12/27.
 */
public interface RedisKeyConstants {

    String BASE_KEY = "xy-open-api:";

    /**
     * 存放用户的登录状态信息+uid
     */
    String JWT_TOKEN = "jwt_token";

    /**
     * 存放用户手机验证码
     * 30分钟过期
     */
    String VERIFY_CODE = "verify_code";

    /**
     * 微信授权的access_token
     */
    String ACCESS_TOKEN = BASE_KEY + "oauth2:access_token";

    /**
     * 短信防刷
     */
    String GAP_KEY = BASE_KEY + "sms:gap:";

}
