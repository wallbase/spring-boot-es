package com.xeiot.open.service.user;

import com.xeiot.open.model.UserInfo;
import com.xeiot.open.rest.Page;
import com.xeiot.open.rest.Pageable;

/**
 * 用户管理
 * Created by wangkun23 on 2018/12/26.
 */
public interface UserService {

    /**
     * 根据用户id查询
     *
     * @param userId
     * @return
     */
    public UserInfo findById(Long userId);

    /**
     * 根据用户名查询
     *
     * @param username
     * @return
     */
    public UserInfo findByUsername(String username);

    /**
     * 根据用户邮箱查询
     *
     * @param email
     * @return
     */
    public UserInfo findByEmail(String email);


    /**
     * 根据用户手机号码查询
     *
     * @param mobile
     * @return
     */
    public UserInfo findByMobile(String mobile);

    /**
     * 用户列表分页查询
     *
     * @param pageable
     * @return
     */
    public Page<UserInfo> findByPage(String username, Pageable pageable);
}
