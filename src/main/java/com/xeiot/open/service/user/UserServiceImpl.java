package com.xeiot.open.service.user;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.xeiot.open.mapper.UserInfoMapper;
import com.xeiot.open.model.UserInfo;
import com.xeiot.open.rest.Page;
import com.xeiot.open.rest.Pageable;
import com.xeiot.open.utils.PrivacyDimmer;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户管理
 * Created by wangkun23 on 2018/12/26.
 */
@Service
public class UserServiceImpl implements UserService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    UserInfoMapper userInfoMapper;

    /**
     * 根据用户id查询
     *
     * @param userId
     * @return
     */
    @Override
    public UserInfo findById(Long userId) {
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
        return userInfo;
    }

    /**
     * 根据用户名查询
     *
     * @param username
     * @return
     */
    @Override
    public UserInfo findByUsername(String username) {
        UserInfo userInfo = null;
        if (username.contains("@")) {
            userInfo = userInfoMapper.findByEmail(username);
        } else {
            userInfo = userInfoMapper.findByMobile(username);
        }
        return userInfo;
    }

    /**
     * 根据用户邮箱查询
     *
     * @param email
     * @return
     */
    @Override
    public UserInfo findByEmail(String email) {
        UserInfo userInfo = userInfoMapper.findByEmail(email);
        return userInfo;
    }

    /**
     * 根据用户手机号码查询
     *
     * @param mobile
     * @return
     */
    @Override
    public UserInfo findByMobile(String mobile) {
        UserInfo userInfo = userInfoMapper.findByMobile(mobile);
        return userInfo;
    }

    /**
     * 用户列表分页查询
     *
     * @param pageable
     * @return
     */
    @Override
    public Page<UserInfo> findByPage(String username, Pageable pageable) {
        PageHelper.startPage(pageable.getPageNumber(), pageable.getPageSize());
        List<UserInfo> list = userInfoMapper.findList(username);
        for (UserInfo userInfo : list) {
            /**
             * 把手机号蒙面处理
             */
            PrivacyDimmer.dim(userInfo);
        }
        PageInfo<UserInfo> pageInfo = new PageInfo<>(list);
        return new Page<>(pageInfo.getList(), pageInfo.getTotal(), pageable);
    }
}
