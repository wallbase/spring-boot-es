package com.xeiot.open.api.v1;

import com.xeiot.open.api.v1.request.RxUser;
import com.xeiot.open.model.UserInfo;
import com.xeiot.open.rest.ResponseData;
import com.xeiot.open.service.user.UserService;
import com.xeiot.open.utils.ResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 用户管理接口
 * Created by wangkun23 on 2018/12/25.
 */
@RestController
@RequestMapping("/api/v1/user")
public class UserApi {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    UserService userService;

    /**
     * 获取用户基本信息
     *
     * @return
     */
    @GetMapping("/info/{userId}")
    public ResponseData info(@PathVariable Long userId) {
        UserInfo userInfo = userService.findById(userId);
        logger.info("userInfo {}", userInfo.getDeviceId());
        ResponseData result = ResponseData.success();
        result.setData(userInfo);
        return result;
    }


    /**
     * 验证长度测试
     *
     * @param rxUser
     * @param bindingResult
     * @return
     */
    @PostMapping("/update")
    public ResponseData update(@Valid @RequestBody RxUser rxUser,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseUtils.valid(bindingResult);
        }
        return ResponseData.success();
    }
}
