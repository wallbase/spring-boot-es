package com.xeiot.open.api.v1.response;

import com.xeiot.open.model.UserInfo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.Date;

/**
 * 返回给客户端的数据封装
 * Created by wangkun23 on 2018/12/26.
 */
@ToString
@NoArgsConstructor
public class RpUser {

    @Setter
    @Getter
    private Long id;

    @Setter
    @Getter
    private String name;

    @Setter
    @Getter
    private String cardId;

    @Setter
    @Getter
    private String phone;

    @Setter
    @Getter
    private Integer sex;

    @Setter
    @Getter
    private Integer type;

    @Setter
    @Getter
    private Date birthday;

    /**
     * 用户头像
     */
    @Setter
    @Getter
    private String avatar;

    /**
     * 用户头像全路径
     */
    @Setter
    @Getter
    private String fullPathAvatar;

    /**
     * 已加入多少天
     */
    @Setter
    @Getter
    private Integer joinDays;

    /**
     * 备注信息
     */
    @Setter
    @Getter
    private String remark;

    @Setter
    @Getter
    private String email;

    @Setter
    @Getter
    private String faceUrl;

    public RpUser(UserInfo userInfo) {
        BeanUtils.copyProperties(userInfo, this);
        if (userInfo.getBirthday() != null) {
            setBirthday(new Date(userInfo.getBirthday()));
        }
    }
}
