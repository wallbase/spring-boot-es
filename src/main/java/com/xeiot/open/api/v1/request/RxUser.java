package com.xeiot.open.api.v1.request;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Size;

/**
 * Created by wangkun23 on 2018/12/22.
 */
public class RxUser {

    @Setter
    @Getter
    @Length(max = 3)
    private String username;
}
