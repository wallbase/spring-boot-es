package com.xeiot.open.mapper;

import com.xeiot.open.model.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * xy_userinfo
 * Created by wangkun23 on 2018/1/17.
 */
@Mapper
public interface UserInfoMapper {
    /**
     * 查询当前数据最大的id 用来生成新的id
     *
     * @return
     */
    public Integer getMaxId();

    int deleteByPrimaryKey(Long uId);

    int insert(UserInfo record);

    UserInfo selectByPrimaryKey(Long uId);

    int update(UserInfo record);

    /**
     * 查询列表
     *
     * @return
     */
    List<UserInfo> findList(@Param("username") String username);

    /**
     * 根据用户邮箱查询
     *
     * @param email
     * @return
     */
    public UserInfo findByEmail(String email);


    /**
     * 根据用户手机号码查询
     *
     * @param mobile
     * @return
     */
    public UserInfo findByMobile(String mobile);
}
