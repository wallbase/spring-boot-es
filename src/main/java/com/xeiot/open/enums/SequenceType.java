package com.xeiot.open.enums;

/**
 * 序列号生产类型，如果是新类型。需要重新申请
 * Created by wangkun23 on 2018/12/19.
 */
public enum SequenceType {


    DEVICE(1000, "设备编号"),

    DEFAULT(0, "默认");


    private Integer key;
    private String value;

    SequenceType(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
