package com.xeiot.open.enums;

/**
 * 性别
 */
public enum Gender {

    /**
     * 男
     */
    male(1),
    /**
     * 女
     */
    female(0);

    private final Integer key;

    private Gender(Integer key) {
        this.key = key;
    }

    public Integer getKey() {
        return key;
    }
}
