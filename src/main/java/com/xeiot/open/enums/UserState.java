package com.xeiot.open.enums;

/**
 * 用户状态信息
 * <p>0删除1启用2禁用</p>
 * Created by wangkun23 on 2018/12/27.
 */
public interface UserState {

    /**
     * 0删除
     */
    public Integer DELETED = 0;

    /**
     * 1启用
     */
    public Integer ACTIVE = 1;

    /**
     * 2禁用
     */
    public Integer LOCKED = 2;
}
