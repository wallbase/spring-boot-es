package com.xeiot.open;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 众鼎配置
 */
@SpringBootApplication
public class SmartCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartCarApplication.class, args);
	}

}
