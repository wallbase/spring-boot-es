package com.xeiot.open.result;

import com.xeiot.open.rest.ResponseCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 服务接口通用返回结果
 * Created by wangkun23 on 2018/12/19.
 */
@ToString
public class ServiceResult<T> implements Serializable {

    @Setter
    @Getter
    private ResponseCode code;

    @Setter
    @Getter
    private Boolean success;

    @Setter
    @Getter
    private T result;

    public ServiceResult(boolean success) {
        this.code = ResponseCode.SUCCESS;
        this.success = success;
    }

    public ServiceResult(boolean success, ResponseCode code) {
        this.success = success;
        this.code = code;
    }

    public ServiceResult(boolean success, ResponseCode code, T result) {
        this.success = success;
        this.code = code;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public static <T> ServiceResult<T> success() {
        return new ServiceResult<>(true);
    }

    public static <T> ServiceResult<T> of(T result) {
        ServiceResult<T> serviceResult = new ServiceResult<>(true);
        serviceResult.setResult(result);
        return serviceResult;
    }

    /**
     * 在返回时定义具体错误
     *
     * @param code
     * @param <T>
     * @return
     */
    public static <T> ServiceResult<T> error(ResponseCode code) {
        return new ServiceResult<>(false, code);
    }

    public static <T> ServiceResult<T> notFound(ResponseCode code) {
        return new ServiceResult<>(false, code);
    }
}
