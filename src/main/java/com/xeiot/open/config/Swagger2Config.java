package com.xeiot.open.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * api接口自动生成管理
 *
 * @seelink http://blog.csdn.net/catoop/article/details/50668896
 * Created by wangkun23 on 2018/12/24.
 */
@Configuration
@EnableSwagger2
@EnableAutoConfiguration
/**
 * 在生产环境不开启
 */
@Profile({"dev", "local"})
public class Swagger2Config {

    @Bean
    public Docket createRestApi() {
        ArrayList<SecurityScheme> auth = new ArrayList<SecurityScheme>(1);
        auth.add(new ApiKey("Authorization", "", "header"));

        return new Docket(DocumentationType.SWAGGER_2)
                .securitySchemes(auth)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xeiot.open.api"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("星翼智能车控Api接口")
                .description("星翼智能车控app后端接口描述文档")
                .contact(new Contact("王坤", "https://www.xeiot.com", "wangkun@baota.io"))
                .version("1.0")
                .build();
    }

}
