package com.xeiot.open.config;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

@Configuration
public class ElasticsearchConfig {

    /**
     * elk集群地址
     */
    @Value("${elastic.host}")
    private String host;

    /**
     * 端口
     */
    @Value("${elastic.port}")
    private Integer port;

    /**
     * 集群名称
     */
    @Value("${elastic.clusterName}")
    private String clusterName;

    /**
     * 连接池
     */
    @Value("${elastic.pool}")
    private String poolSize;

    /**
     * Bean name default  函数名字
     *
     * @return
     */
    @Bean(name = "transportClient")
    public TransportClient transportClient() throws UnknownHostException {
        TransportClient transportClient = null;
        Settings esSetting = Settings.builder()
                .put("cluster.name", clusterName) //集群名字
                .put("client.transport.sniff", true)//增加嗅探机制，找到ES集群
                .put("thread_pool.search.size", Integer.parseInt(poolSize))//增加线程池个数，暂时设为5
                .build();
        //配置信息Settings自定义
        transportClient = new PreBuiltTransportClient(esSetting);
        transportClient.addTransportAddresses(new TransportAddress(new InetSocketAddress(host, port)));
        return transportClient;
    }
}
