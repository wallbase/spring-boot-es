package com.xeiot.open.utils;

import com.xeiot.open.rest.ResponseCode;
import com.xeiot.open.rest.ResponseData;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * 返回信息封装常用逻辑
 * Created by wangkun23 on 2018/12/25.
 */
public class ResponseUtils {

    private ResponseUtils() {
    }

    /**
     * 返回字段验证不通过错误消息
     *
     * @param bindingResult bindingResult
     * @return 错误消息
     */
    public static ResponseData valid(BindingResult bindingResult) {
        ResponseData result = ResponseData.error(ResponseCode.ERROR_VALID);
        FieldError error = bindingResult.getFieldErrors().get(0);
        String msg = error.getField() + " " + error.getDefaultMessage();
        result.setMsg(msg);
        return result;
    }
}
