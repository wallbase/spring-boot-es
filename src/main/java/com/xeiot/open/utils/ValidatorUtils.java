package com.xeiot.open.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 通用的验证器
 * Created by wangkun23 on 2018/12/25.
 */
public class ValidatorUtils {

    private ValidatorUtils() {
    }

    /**
     * 判断是否是手机号
     *
     * @param mobile
     * @return
     */
    public static boolean isMobile(String mobile) {
        String regexp = "^1(3|4|5|7|8|9)\\d{9}$";
        Pattern p = Pattern.compile(regexp);
        Matcher m = p.matcher(mobile);
        return m.matches();
    }


    /**
     * 从一段字符串中提取第一个手机号码
     *
     * @param words
     * @return
     */
    public static String fetchFirstMobile(String words) {
        //不要以$客户给的数据不确定的
        String regexp = "1(3|4|5|7|8|9)\\d{9}";
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(words);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return null;
    }
}
