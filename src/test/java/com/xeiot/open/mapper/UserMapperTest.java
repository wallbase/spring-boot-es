package com.xeiot.open.mapper;

import com.xeiot.open.model.UserInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by wangkun23 on 2019/4/9.
 */
@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
public class UserMapperTest {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    UserInfoMapper userInfoMapper;

    @Test
    public void selectByPrimaryKey() {
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(19782L);
        logger.info("{}", userInfo);
    }
}
